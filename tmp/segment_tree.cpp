/*
 * Segment Tree
 * Construction: O(n)
 * Update:       O(log n)
 * Query:        O(log n)
 */

#include <iostream>
#include <vector>
#include <climits>
using namespace std;

class SegmentTree {
    const int N; // array size
    const int M; // initial value

    int (*f)(int, int); // operation

public:
    vector<int> t; // tree

    SegmentTree(int N, int M, int (*f)(int, int)) : N(N), M(M), f(f), t(2 * N, M) {}

    void build() { // build the tree
        for (int i = N - 1; i > 0; --i)
            t[i] = f(t[i << 1], t[i << 1 | 1]);
    }

    void update(int k, int val) { // set val at k th
        k += N;
        for (t[k] = val; k > 1; k >>= 1)
            t[k >> 1] = f(t[k], t[k ^ 1]);
    }

    int query(int l, int r) { // query on interval [l, r)
        int res = M;
        for (l += N, r += N; l < r; l >>= 1, r >>= 1) {
            if (l & 1) res = f(res, t[l++]);
            if (r & 1) res = f(res, t[--r]);
        }
        return res;
    }
};

void rmq() {
    int n, q;
    cin >> n >> q;
    SegmentTree T(n, INT_MAX, [](int a, int b) { return min(a, b); });
    T.build();

    while (q--) {
        int com, x, y;
        cin >> com >> x >> y;
        if (com)
            cout << T.query(x, y + 1) << endl;
        else
            T.update(x, y);
    }
}

void rsq() {
    int n, q;
    cin >> n >> q;
    SegmentTree T(n, 0, [](int a, int b) { return a + b; });
    T.build();

    while (q--) {
        int com, x, y;
        cin >> com >> x >> y;
        if (com) cout << T.query(x, y + 1) << endl;
        else T.update(x, T.t[x + n] + y);
    }
}

int main() {
    rmq();
    //rsq();
}

