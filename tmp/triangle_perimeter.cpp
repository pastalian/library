// O(n^2 log n)

#include <iostream>

using namespace std;

int n;
#define MAX_N 100
int a[MAX_N];

void solve() {
    int perimeter = 0;
    for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)
            for (int k = j + 1; k < n; ++k) {
                int length = a[i] + a[j] + a[k];
                int longest = max(a[i], max(a[j], a[k]));
                int rest = length - longest;

                if (longest < rest) perimeter = max(perimeter, length);
            }
    cout << perimeter << endl;
}

int main() {
    cin >> n;
    for (int i = 0; i < n; ++i) cin >> a[i];

    solve();
}

