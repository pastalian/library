// O(2^n)

#include <algorithm>
#include <iostream>

using namespace std;

int n, k;
#define MAX_N 20
int a[MAX_N];

bool dfs(int i, int sum) {
    if(i==n) return sum == k;

    return dfs(i+1, sum) || dfs(i + 1, sum + a[i]);
}

void solve() {
    if(dfs(0, 0)) cout << "Yes" << endl;
    else cout << "No" << endl;
}

int main() {
    cin >> n;
    for(int i=0;i<n;++i) cin >> a[i];
    cin >> k;

    solve();
}

