#include <functional>

template<class T>
using V = typename std::iterator_traits<T>::value_type;
template<class T>
long long two_pointers(T first, T last,
                       V<T> identity,
                       V<T> query,
                       std::function<V<T>(V<T>, V<T>)> op,
                       std::function<bool(V<T>, V<T>)> q_op) {
  using V = typename std::iterator_traits<T>::value_type;

  long long ret = 0;
  const T kRight = first;
  V val = identity;

  for (const T kLeft = first; kLeft != last; ++kLeft) {
    while (kRight != last && q_op(op(val, kRight), query)) {
      val = op(val, kRight);
      ++kRight;
    }

  }

  return ret;
}

