/*
 * Lazy Segment Tree
 * Construction: O(n)
 * Update:       O(log n)
 * Query:        O(log n)
 *
 * Memory Usage: 3n
 */

#include <vector>
#include <cstdio>
using namespace std;

// Currently f(a, b) := a + b
class SegmentTree {
    const int N; // array size
    const int M; // initial value
    const int O; // initial value of the query
    const int H; // height of the tree, the highest significant bit in N

    int (*f)(int, int); // query operation
    int (*g)(int, int); // update operation

public:
    vector<int> t; // tree
    vector<int> d; // delayed tree

    SegmentTree(int N, int M, int O, int (*f)(int, int), int (*g)(int, int)) :
            N(N), M(M), O(O), H(sizeof(int) * 8 - __builtin_clz(N)),
            f(f), g(g), t(2 * N, M), d(N) {}

    void init() { // init tree from leaves
        for (int i = N - 1; i > 0; --i)
            t[i] = f(t[i << 1], t[i << 1 | 1]);
    }

    void set(int k, int val) { // direct edit
        t[k + N] = val;
    }

    void apply(int p, int val, int k) { // propagate helper
        t[p] = g(val * k, t[p]);
        if (p < N) d[p] = g(val, d[p]);
    }

    void calc(int p, int k) { // build helper
        if (d[p]) t[p] = d[p] * k;
        else t[p] = f(t[p << 1], t[p << 1 | 1]);
    }

    void build(int p) { // build from p th node to root
        int k = 2;
        for (p += N; p > 1; k <<= 1) {
            p >>= 1;
            calc(p, k);
        }
    }

    void propagate(int p) { // propagate to p th node from root
        p += N;
        for (int s = H, k = 1 << (H - 1); s > 0; --s, k >>= 1) {
            int i = p >> s;
            if (d[i] != 0) {
                apply(i << 1, d[i], k);
                apply(i << 1 | 1, d[i], k);
                d[i] = 0;
            }
        }
    }

    void update(int l, int r, int val) { // set val at interval [l, r)
        propagate(l);
        propagate(r - 1); // make sure old propagation is completed

        int k = 1; // interval level
        int l0 = l, r0 = r;
        l += N, r += N;

        for (; l < r; l >>= 1, r >>= 1, k <<= 1) {
            if (l & 1) apply(l++, val, k);
            if (r & 1) apply(--r, val, k);
        }

        build(l0);
        build(r0 - 1);
    }

    int query(int l, int r) { // query on interval [l, r)
        propagate(l);
        propagate(r - 1);
        int res = O;
        for (l += N, r += N; l < r; l >>= 1, r >>= 1) {
            if (l & 1) res = f(t[l++], res);
            if (r & 1) res = f(t[--r], res);
        }
        return res;
    }
};

void put_uint(int n) {
    if (!n) {
        putchar_unlocked('0');
        return;
    }
    char buf[11];
    int i = 0;
    while (n) buf[i++] = (char) (n % 10 + '0'), n /= 10;
    while (i--)putchar_unlocked(buf[i]);
}

int get_uint() {
    int n = 0;
    int c = getchar_unlocked();
    if (c < 48 || 57 < c) return c;
    while (47 < c && c < 58) n = 10 * n + (c & 0xf), c = getchar_unlocked();
    return n;
}

void test() {
    int n = get_uint();
    int q = get_uint();
    SegmentTree T(n, 0, 0,
                  [](int a, int b) { return a + b; },
                  [](int a, int b) { return a + b; }
    );

    while (q--) {
        int type = get_uint();
        if (type) {
            int s = get_uint();
            int t = get_uint();
            put_uint(T.query(s - 1, t));
            putchar_unlocked('\n');
        } else {
            int s = get_uint();
            int t = get_uint();
            int x = get_uint();
            T.update(s - 1, t, x);
        }
    }
}

int main() {
    test();
}

