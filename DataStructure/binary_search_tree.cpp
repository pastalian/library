#include<iostream>
template<class T>
class BST {
private:
    struct Node {
        T key;
        Node* left;
        Node* right;
        Node(T key) {
            this->key = key;
            this->left = nullptr;
            this->right = nullptr;
        }
        Node(T key, Node left, Node right) {
            this->key = key;
            this->left = left;
            this->right = right;
        }
    };
    Node* root = nullptr;
public:
    ~BST() {
        makeEmpty(root);
    }
    void insert(T key) {
        root = insertHelper(key, root);
    }
    void remove(T key) {
        removeHelper(key, root);
    }
    bool find(T key) {
        return findHelper(key, root);
    }
    void inorder() {
        inorderHelper(root);
    }
    void preorder() {
        preorderHelper(root);
    }
private:
    Node* insertHelper(T key, Node* root) {
        if (root == nullptr) return new Node(key);
        if (key < root->key) root->left = insertHelper(key, root->left);
        else root->right = insertHelper(key, root->right);
        return root;
    }
    Node* removeHelper(T key, Node* root) {
        if (root == nullptr) return nullptr;
        if (key < root->key) root->left = removeHelper(key, root->left);
        else if (key > root->key) root->right = removeHelper(key, root->right);
        else if (root->left && root->right) {
            Node* temp = getMin(root->right);
            root->key = temp->key;
            root->right = removeHelper(root->key, root->right);
        }
        else {
            Node* temp = root;
            if (root->left == nullptr) root = root->right;
            else if (root->right == nullptr) root = root->left;
            delete temp;
        }
        return root;
    }
    bool findHelper(T key, Node* root) {
        if (root == nullptr) return false;
        if (key == root->key) return true;
        if (key < root->key) return findHelper(key, root->left);
        else return findHelper(key, root->right);
    }
    Node* getMin(Node* root) {
        if (root == nullptr) return nullptr;
        if (root->left == nullptr) return root;
        return getMin(root->left);
    }
    void makeEmpty(Node* root) {
        if (root == nullptr) return;
        makeEmpty(root->left);
        makeEmpty(root->right);
        delete root;
    }
    void inorderHelper(Node* root) {
        if (root == nullptr) return;
        inorderHelper(root->left);
        std::cout << " " << root->key;
        inorderHelper(root->right);
    }
    void preorderHelper(Node* root) {
        if (root == nullptr) return;
        std::cout << " " << root->key;
        preorderHelper(root->left);
        preorderHelper(root->right);
    }
};
