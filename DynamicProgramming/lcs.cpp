/*
 * Longest Common Subsequence
 * O(NM)
 *
 * !TODO
 * getSubsequence
 */

#include<string>
int lcs(std::string s, std::string t) {
    const int M = s.size(), N = t.size();
    int dp[M + 1][N + 1] = {};
    for (int i = 0; i < M; ++i)
        for (int j = 0; j < N; ++j)
            if (s[i] == t[j]) dp[i + 1][j + 1] = dp[i][j] + 1;
            else dp[i + 1][j + 1] = std::max(dp[i + 1][j], dp[i][j + 1]);
    return dp[M][N];
}
