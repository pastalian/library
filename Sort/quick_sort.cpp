/*
 * Quicksort
 * O(NlogN) average, unstable
 * O(N^2)   worst
 */

#include<algorithm>
#include<functional>
#include<iterator>

template<class T, class C = std::less<>>
void quickSort(T first, T last, C cmp = C()) {
    const auto N = std::distance(first, last);
    if (N <= 1) return;
    const auto pivot = *std::next(first, N / 2);
    const auto mid1 = std::partition(first, last, [=](const auto& elem) {return cmp(elem, pivot);});
    const auto mid2 = std::partition(mid1, last, [=](const auto& elem) {return !cmp(pivot, elem);});
    quickSort(first, mid1, cmp);
    quickSort(mid2, last, cmp);
}
