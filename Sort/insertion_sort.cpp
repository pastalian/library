/*
 *  Insertion Sort
 *  O(N^2), stable
 */

#include<algorithm>
#include<functional>
#include<iterator>

template<class T, class C = std::less<>>
void insertionSort(T first, T last, C cmp = C()) {
    if (std::distance(first, last) <= 1) return;
    for (auto it = first; it != last; ++it) {
        auto const insertion = std::upper_bound(first, it, *it, cmp);
        std::rotate(insertion, it, std::next(it));
    }
}
