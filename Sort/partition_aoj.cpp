#include<algorithm>
#include<functional>
template<class T, class C = std::less<>>
T partition(T first, T last, C cmp = C()) {
    const auto pivot = last[-1];
    T bound = first - 1;
    for (T it = first; it != last; ++it)
        if (!cmp(pivot, *it)) std::iter_swap(it, ++bound);
    return bound;
}
