#include<algorithm>
#include<chrono>
#include<iostream>
#include<random>
#define ARRAY_SIZE 1000000
using namespace std;

template<class T, class C = less<>>void measureF(void(*sortFunc)(T,T,C), T first, T last, C cmp = C()) {
    vector<int>v(ARRAY_SIZE);
    copy(first, last, v.begin());
    chrono::system_clock::time_point start, end;
    start = chrono::system_clock::now();
    sortFunc(v.begin(), v.end(), cmp);
    end = chrono::system_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
}
int main() {
    vector<int>v(ARRAY_SIZE);
    mt19937 mt(random_device{}());
    generate_n(v.begin(), ARRAY_SIZE, mt);
    measureF(sort,v.begin(),v.end());
    //measureF(sortFunc, v.begin(), v.end());
}
