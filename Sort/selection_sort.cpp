/*
 *  Selection Sort
 *  O(N^2), stable
 */

#include<algorithm>
#include<functional>
#include<iterator>

template<class T,class C=std::less<>>
void selectionSort(T first, T last, C cmp = C()) {
    if (std::distance(first, last) <= 1) return;
    for (auto it = first; it != last; ++it) {
        const auto selection = std::min_element(it, last, cmp);
        std::iter_swap(selection, it);
    }
}
