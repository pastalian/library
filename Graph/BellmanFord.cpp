#include<bits/stdc++.h>
#define INF 1e18
#define int long long
using namespace std;
typedef pair<int,int> P;

struct Graph{
  Graph(int n):E(n),d(n),N(n){};
  vector<vector<P>>E;
  vector<int>d;
  int N;
  void add_edge(int a,int b,int c){
    E[a].emplace_back(b,c);
  }
  bool bellmanFord(int s,int t){
    fill(d.begin(),d.end(),INF);
    d[s]=0;
    for(int i=0;i<N;++i)
      for(int j=0;j<N;++j)
        for(P& p:E[j])
          if(d[j]!=INF&&d[j]+p.second<d[p.first]){
            d[p.first]=d[j]+p.second;
            if(i==N-1&&p.first==t)return false;
            //if check after destination route
            // if(i==N-1)return false;
          }
    return true;
  }
  int get_cost(int n){
    return d[n];
  }
};

