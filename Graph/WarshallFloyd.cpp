#include<bits/stdc++.h>
#define INF 1e9
using namespace std;

struct Graph{
  Graph(int n):d(n,vector<int>(n,INF)),N(n){
    for(int i=0;i<n;++i)d[i][i]=0;
  }
  vector<vector<int>>d;
  int N;
  void add_edge(int a,int b,int c){
    d[a][b]=c;
  }
  void warshallFloyd(){
    for(int k=0;k<N;++k)
      for(int i=0;i<N;++i)
        for(int j=0;j<N;++j)
          d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
  }
};

main(){
  int n,m,a,b,c,x=INF;
  cin>>n>>m;
  Graph E(n);
  for(int i=0;i++<m;E.add_edge(--a,--b,c),E.add_edge(b,a,c))cin>>a>>b>>c;
  E.warshallFloyd();
  for(int i=0;i<n;++i)
    x=min(x,*max_element(E.d[i].begin(),E.d[i].end()));
  cout<<x<<endl;
}


/*******************************

ABC012_D
http://abc012.contest.atcoder.jp/tasks/abc012_4

********************************/
