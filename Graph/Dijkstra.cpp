/*
 * Dijkstra
 * O(E+VlogV)
 */

#include<functional>
#include<queue>
#include<vector>

struct Graph {
    Graph(int n) :E(n), d(n) {}
    using P = std::pair<int, int>;
    std::vector<std::vector<P>>E;
    std::vector<int>d;
    void addEdge(int a, int b, int c) { E[a].emplace_back(b, c); }
    void dijkstra(int s) {
        std::priority_queue<P, std::vector<P>, std::greater<>>Q;
        fill(d.begin(), d.end(), 1 << 30);
        Q.push({ d[s] = 0,s });
        while (!Q.empty()) {
            P e = Q.top(); Q.pop();
            if (e.first > d[e.second]) continue;
            for (P& a : E[e.second]) {
                int x = a.first, y = a.second + e.first;
                if (y < d[x]) d[x] = y, Q.push({ y,x });
            }
        }
    }
    int getCost(int n) { return d[n]; }
};
