## ダイクストラ法

>ダイクストラ法（だいくすとらほう、英: Dijkstra's algorithm）はグラフ理論における辺の重みが非負数の場合の単一始点最短経路問題を解くための最良優先探索によるアルゴリズムである。辺の重みに負数を含む場合はベルマン-フォード法などが使える。辺の重みが全て同一の非負数の場合は幅優先探索が速く、線形時間で最短路を計算可能である。また、無向グラフで辺の重みが正整数の場合は、Thorupのアルゴリズムによって線形時間での計算が可能であるが、実用性はあまり高くない。

![illustration](https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)

```cpp
    /* グラフG=(V,E)、頂点uからvへの長さlength(u,v)、開始頂点sを受け取る
       E(v)=(u,length(v,u))、d(v)は開始頂点からの最短距離の長さ */
       
    void dijkstra(int s){
      priority_queue<P,vector<P>,greater<P>>q;
      
      // 初期化: 頂点v∈Vに対し、d(v)←(v=sならば0、それ以外は∞)
      //        q←(d(s),s)
      fill(d.begin(),d.end(),INF);
      q.push({d[s]=0,s});
      
      // 本計算
      while(q.size()){
        // qからd(u)が最小である頂点uを取り出す
        P p=q.top();q.pop();
        // 枝刈り: 既にd(u)が更新されている場合
        if(p.first>d[p.second])continue;
        for(P& a:E[p.second]){
          int x=a.first,y=a.second+p.first;
          // 最短距離更新
          if(y<d[x])d[x]=y,q.push({y,x});
        }
      }
    }
```
