#include <filesystem>

std::vector<std::string> ListFilePath(const std::string &root) {
  std::vector<std::string> paths;

  for (const auto &a : std::filesystem::recursive_directory_iterator(root)) {
    if (!a.is_directory()) {
//      if (a.path().extension() == ".png")
      paths.emplace_back(a.path());
    }
  }

  return paths;
}
