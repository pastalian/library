#include<algorithm>
#include<iostream>
#include<random>
#include<string>
#include<vector>
using namespace std;

class SampleData{
public:
    void setData(int key, double data, string name) {
        _key = key, _data = data, _name = name;
    }
    bool operator<(const SampleData& rhs) const {
        return _data == rhs._data ? _name < rhs._name : _data < rhs._data;
    }
    bool operator>(const SampleData& rhs) const {
        return _data == rhs._data ? _name > rhs._name : _data > rhs._data;
    }
    friend ostream& operator<<(ostream& out, const SampleData& s) {
        return out << s._key << " " << s._data << " " << s._name;
    }
    friend istream& operator>>(istream& in, SampleData& s) {
        in >> s._key >> s._data >> s._name;
        return in;
    }
private:
    int _key;
    double _data;
    string _name;
};

int main() {
    random_device rnd;
    vector<SampleData> v(10);
    vector<string>names = { "pasta","dummy","white","black" };
    for (int i = 0; i < 10; ++i) v[i].setData(i, rnd() % 100, names[rnd() % 4]);
    //for (auto& a : v) cin >> a;
    for (auto& a : v) cout << a << endl;
}
