#include<functional>
template<class RandomIt, class Comp = std::less<>>
RandomIt sample(RandomIt first, RandomIt last, Comp cmp = Comp()) {
    using T = typename RandomIt::value_type;
    T right = last[-1];
    T left = *first;
    for (RandomIt it = first; it != last; ++it)
        if (cmp(*it, right)) return it;
    return last;
}
