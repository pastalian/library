#include<string>
#include<vector>

void erato(bool prime[], int N) {
    for (int i = 0; i < N; ++i) prime[i] = true;
    prime[0] = prime[1] = false;
    for (int i = 2; i*i <= N; ++i)
        if (prime[i])
            for (int j = i*2; j <= N; j+=i) prime[j] = false;
}
template<class T>
bool isPrime(T n) {
    if (n == 2) return true;
    if (n < 2 || n % 2 == 0) return false;
    for (T i = 3; i*i <= n; i += 2)
        if (n%i == 0) return false;
    return true;
}
template<class T>
std::vector<T> getPrime(T n) {
    // !TODO
}
bool isPrimeUnix(std::string s){
    FILE *fp;
    if((fp=popen(("factor "+s+"|awk 'NF==2{print$2}'").c_str(),"r"))==nullptr) exit(1);
    char c[100];
    fgets(c,sizeof(c),fp);
    pclose(fp);
    std::string t=c;
    t.pop_back();
    if(t==s) return true;
    return false;
}
