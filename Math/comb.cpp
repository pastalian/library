#include <algorithm>

const int mod = 1000000007;

long long power_mod(long long n, long long p) { // n^p % mod
    long long r = 1;
    while (p) {
        if (p & 1) {
            r = r * n % mod;
        }
        n = n * n % mod;
        p >>= 1;
    }
    return r;
}

long long comb_mod(long long n, long long k) {  // nCk % mod
    if (n < 0 || k < 0 || n < k) return 0;
    long long num = 1, den = 1;
    k = std::min(k, n - k);
    for (int i = 1; i <= k; ++i) {
        num = num * n % mod;
        den = den * i % mod;
        n--;
    }
    return num * power_mod(den, mod - 2) % mod;
}
