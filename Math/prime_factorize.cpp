#include<vector>
#include<string>
template<class T>
std::vector<T> factorize(T N){
    std::vector<T> factor;
    for(unsigned i=2;i*i<=N;++i)
        while(N%i==0){
            factor.push_back(i);
            N/=i;
        }
    if(N>1) factor.push_back(N);
    return factor;
}
std::vector<long long> factorizeUnix(std::string s){
    FILE *fp;
    if((fp=popen(("factor "+s+"|awk -F'[:]' '{print$2}'").c_str(),"r"))==nullptr) exit(1);
    std::vector<long long> factor;
    long long n;
    while(~fscanf(fp,"%lld",&n)) factor.push_back(n);
    return factor;
}
