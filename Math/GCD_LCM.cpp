int gcd(int x,int y){
  return y?gcd(y,x%y):x;
}

int lcm(int x,int y){
  return x/gcd(x,y)*y;
}

/*************************
gcd(a,b,c)=gcd(gcd(a,b),c)
*************************/
