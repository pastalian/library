#include <algorithm>
#include <cmath>
#include <tuple>
#include <vector>

// T: sorted by x-axis
template<class T>
std::tuple<double,
           typename std::iterator_traits<T>::value_type,
           typename std::iterator_traits<T>::value_type>
ClosestPair(T left, T right) {
    using V = typename std::iterator_traits<T>::value_type;

    const int n = std::distance(left, right);

    if (n == 1) {
        return std::make_tuple(std::numeric_limits<double>::max(), *left, *left);
    }

    T mid = std::next(left, n/2);
    double x = std::get<0>(*mid);
    auto d = std::min(ClosestPair(left, mid), ClosestPair(mid, right));
    std::inplace_merge(left, mid, right,
                       [](const V &a, const V &b) { return std::get<1>(a) <= std::get<1>(b); });

    std::vector<V> b;
    b.reserve(n);
    for (T it = left; it != right; ++it) {
        if (std::abs(std::get<0>(*it) - x) >= std::get<0>(d)) continue;

        for (auto jt = b.rbegin(); jt != b.rend(); ++jt) {
            if (std::abs(std::get<0>(*it) - x) >= std::get<0>(d)) continue;
            double dx = std::get<0>(*it) - std::get<0>(*jt);
            double dy = std::get<1>(*it) - std::get<1>(*jt);
            if (dy >= std::get<0>(d)) break;
            if (sqrt(dx*dx + dy*dy) < std::get<0>(d)) {
                d = std::make_tuple(sqrt(dx*dx + dy*dy), *it, *jt);
            }
        }
        b.push_back(*it);
    }
    return d;
}

void Test() {
    int n;
    scanf("%d", &n);
    std::vector<std::tuple<double, double, int>> v(n);
    for (int i = 0; i < n; ++i) {
        double a, b;
        scanf("%lf%lf", &a, &b);
        v[i] = std::make_tuple(a, b, i);
    }
    std::sort(v.begin(), v.end());
    auto ans = ClosestPair(v.begin(), v.end());
    printf("%.7f %d %d\n",
           std::get<0>(ans),
           std::get<2>(std::get<1>(ans)),
           std::get<2>(std::get<2>(ans)));
}
