#include <complex>
#include <iostream>
#include <vector>

// T: float, double, long double
template<class T>
std::complex<T> MaxCoverCircle(std::vector<std::complex<T>> &p, T radius) {
  const int n = p.size();
  const T eps = 1e-6;
  int num_points = 1;
  std::complex<T> center_max(p[0]);

  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      auto diff = p[i] - p[j];
      T dist = abs(diff);

      if (dist - 2*radius > eps) continue;

      auto mid = p[j] + (diff/(T) 2);
      auto mid_unit_normal = (diff*std::complex<T>(0, 1))/dist;
      auto mid_to_center = sqrt(radius - dist*dist/(T) 4)*mid_unit_normal;

      for (int k = 0; k < 2; ++k) {
        mid_to_center *= -1;
        auto center = mid + mid_to_center;

        int cnt = 0;
        for (int l = 0; l < n; ++l) {
          auto q = p[l] - center;
          if (q.real()*q.real() + q.imag()*q.imag() - radius*radius < eps) cnt++;
        }

        if (cnt > num_points) {
          num_points = cnt;
          center_max = center;
        }
      }
    }
  }

  return center_max;
}

// https://onlinejudge.u-aizu.ac.jp/problems/1132
void Test() {
  int n;
  while (std::cin >> n, n) {
    std::vector<std::complex<double>> p;
    p.reserve(n);
    for (int i = 0; i < n; ++i) {
      double x, y;
      std::cin >> x >> y;
      p.emplace_back(x, y);
    }
    MaxCoverCircle(p, 1.0);
  }
}
